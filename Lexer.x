{
module Lexer where
}

%wrapper "basic"

$operators = [\+\-\*\/]
$brackets = [\(\)\[\]\{\}]
@num = [0-9]+

tokens :-
  $white+    ;

  @num                   { \s -> Number (read s) }
  [$operators $brackets] { \s -> Symbol (head s) }

{
data Token
  = Symbol Char
  | Number Int
  deriving (Eq, Show)
}
