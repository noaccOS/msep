Lexer.hs: Lexer.x
	alex Lexer.x

Parser.hs: Parser.y
	happy Parser.y

Parser: Parser.hs Lexer.hs
	ghc Parser.hs

Lexer: Lexer.hs
	ghc Lexer.hs

Main: Parser Main.hs
	ghc Main.hs

parse: Parser
	./Parser

lex: Lexer
	./Lexer

clean:
	rm Lexer{,.hi,.hs,.o} Parser{,.hi,.hs,.info,.o} Main{,.hi,.o}
