{
module Parser where
import Lexer
}

%name calc
%tokentype { Token }

%token  num             { Number $$  }
        '+'             { Symbol '+' }
        '-'             { Symbol '-' }
        '*'             { Symbol '*' }
        '/'             { Symbol '/' }
        '('             { Symbol '(' }
        ')'             { Symbol ')' }
        '['             { Symbol '[' }
        ']'             { Symbol ']' }
        '{'             { Symbol '{' }
        '}'             { Symbol '}' }

%%

CurlyExp
  : CurlyExp '+' CurlyHighPriority { PlusE  $1 $3  }
  | CurlyExp '-' CurlyHighPriority { MinusE $1 $3  }
  | CurlyHighPriority              { $1            }

CurlyHighPriority
  : CurlyHighPriority '*' CurlyFactor { TimesE $1 $3  }
  | CurlyHighPriority '/' CurlyFactor { DivE   $1 $3  }
  | CurlyFactor                       { $1            }

CurlyFactor
  : '{' SquareExp '}'  { $2 }
  | MaybeSquareFactor  { $1 }

SquareExp
  : SquareExp        '+' MaybeSquareHighPriority { PlusE  $1 $3 }
  | SquareExp        '-' MaybeSquareHighPriority { MinusE $1 $3 }
  | MaybeRoundFactor '+' SquareHighPriority      { PlusE  $1 $3 }
  | MaybeRoundFactor '-' SquareHighPriority      { MinusE $1 $3 }
  | SquareHighPriority                        { $1           }

SquareHighPriority
  : SquareHighPriority '*' MaybeSquareFactor { TimesE $1 $3 }
  | SquareHighPriority '/' MaybeSquareFactor { DivE   $1 $3 }
  | MaybeRoundFactor   '*' SquareFactor      { TimesE $1 $3 }
  | MaybeRoundFactor   '/' SquareFactor      { DivE   $1 $3 }
  | SquareFactor                             { $1           }

MaybeSquareHighPriority
  : MaybeSquareHighPriority '*' MaybeSquareFactor { TimesE $1 $3  }
  | MaybeSquareHighPriority '/' MaybeSquareFactor { DivE   $1 $3  }
  | MaybeSquareFactor                             { $1            }

SquareFactor
  : '[' RoundExp ']' { $2 }

MaybeSquareFactor
  : SquareFactor     { $1 }
  | MaybeRoundFactor { $1 }

RoundExp
  : RoundExp   '+' MaybeRoundHighPriority { PlusE  $1 $3 }
  | RoundExp   '-' MaybeRoundHighPriority { MinusE $1 $3 }
  | FlatFactor '+' RoundHighPriority      { PlusE  $1 $3 }
  | FlatFactor '-' RoundHighPriority      { MinusE $1 $3 }
  | RoundHighPriority                     { $1           }

RoundHighPriority
  : RoundHighPriority '*' MaybeRoundFactor { TimesE $1 $3 }
  | RoundHighPriority '/' MaybeRoundFactor { DivE   $1 $3 }
  | FlatFactor        '*' RoundFactor      { TimesE $1 $3 }
  | FlatFactor        '/' RoundFactor      { DivE   $1 $3 }
  | RoundFactor                            { $1           }

MaybeRoundHighPriority
  : MaybeRoundHighPriority '*' MaybeRoundFactor { TimesE $1 $3  }
  | MaybeRoundHighPriority '/' MaybeRoundFactor { DivE   $1 $3  }
  | MaybeRoundFactor                            { $1            }

RoundFactor
  : '(' FlatExp ')' { $2 }

MaybeRoundFactor
  : RoundFactor { $1 }
  | FlatFactor  { $1 }

FlatExp
  : FlatExp '+' FlatHighPriority { PlusE  $1 $3  }
  | FlatExp '-' FlatHighPriority { MinusE $1 $3  }
  | FlatHighPriority             { $1            }            

FlatHighPriority
  : FlatHighPriority '*' FlatFactor { TimesE $1 $3  }
  | FlatHighPriority '/' FlatFactor { DivE   $1 $3  }
  | FlatFactor                      { $1            }

FlatFactor
  : num                             { NumE $1        }
  | '-' num                         { NegE (NumE $2) }

{
data Exp
  = PlusE  Exp Exp
  | MinusE Exp Exp
  | TimesE Exp Exp
  | DivE   Exp Exp
  | NegE   Exp
  | NumE   Int
    deriving Show


main:: IO ()
main = interact $ show . runCalc

runCalc :: String -> Exp
runCalc = calc . alexScanTokens

happyError :: [Token] -> a
happyError _ = error "Parse error"
}
