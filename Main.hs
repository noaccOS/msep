module Main where
import Parser (runCalc, Exp(..))

val :: Exp -> Int
val (NumE n) = n
val (NegE e) = -val e
val (DivE   e1 e2) = val e1 `div` val e2
val (TimesE e1 e2) = val e1 *     val e2
val (MinusE e1 e2) = val e1 -     val e2
val (PlusE  e1 e2) = val e1 +     val e2

main = interact $ show . val . runCalc
